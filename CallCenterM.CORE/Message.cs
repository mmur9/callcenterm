﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CallCenterM.CORE
{/// <summary>
/// Clase de dominio de mensaje
/// </summary>
    public class Message
    {
        /// <summary>
        /// identificador del mensaje
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// fecha de creacion del mensaje
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Mensaje de la incidencia
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Usuario que escribe el mensaje
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que escribe el mensaje
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }

        /// <summary>
        /// Incidencia a la que pertenece el mensaje
        /// </summary>
        public Incidence Incidence { get; set; }

        /// <summary>
        /// Identificador de la incidencia a la que pertenece el mensaje
        /// </summary>
        [ForeignKey("Incidence")]
        public int Incidence_Id { get; set; }




    }
}