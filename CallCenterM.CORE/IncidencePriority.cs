﻿namespace CallCenterM.CORE
{
    /// <summary>
    /// Enumerado de prioridad
    /// </summary>
    public enum IncidencePriority
    {
        /// <summary>
        /// Baja
        /// </summary>
        Low = 0,
        /// <summary>
        /// Normal
        /// </summary>
        Average = 1,
        /// <summary>
        /// Alta
        /// </summary>
        High = 2
    }
}