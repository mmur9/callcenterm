﻿namespace CallCenterM.CORE
{
    public enum IncidenceType
    {
        /// <summary>
        /// Software
        /// </summary>
        Software = 1,
        /// <summary>
        /// Hardware
        /// </summary>
        Hardware = 2,
        /// <summary>
        /// Other
        /// </summary>
        Other = 3
    }
}