﻿namespace CallCenterM.CORE
{
    /// <summary>
    /// Enumerado de los posibles estados de una incidencia
    /// </summary>
    public enum IncidenceStatus : int
    {
        /// <summary>
        /// Abierta - nueva
        /// </summary>
        Open = 0,
        /// <summary>
        /// Cerrada
        /// </summary>
        Closed = 1
    }
}