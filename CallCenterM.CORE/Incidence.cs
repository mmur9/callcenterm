﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterM.CORE
{
    /// <summary>
    /// Entidad de dominio de incidencias
    /// </summary>
    public class Incidence
    {
        /// <summary>
        /// Identificador de la incidencia
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del equipo
        /// </summary>
        public string Equipment { get; set; }

        /// <summary>
        /// Fecha de la creacion de la incidencia
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha de cierre de la incidencia
        /// Con el interrogante, permitimos que sea NULL
        /// </summary>
        public DateTime? ClosedDate { get; set; }

        /// <summary>
        /// Nota interna de la incidencia
        /// </summary>
        public string InternalNote { get; set; }

        /// <summary>
        /// Estado de la incidencia
        /// </summary>
        public IncidenceStatus Status { get; set; }

        /// <summary>
        /// Prioridad de la incidencia
        /// </summary>
        public IncidencePriority Priority { get; set; }

        /// <summary>
        /// Tipo de incidencia
        /// </summary>
        public IncidenceType IncenceType { get; set; }

        /// <summary>
        /// Usuario que ha creado la incidencia (objeto applicationuser)
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que ha creado la incidencia
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }

        public virtual List<Message> Messages { get; set; }

     
    }

    
}
