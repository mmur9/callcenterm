﻿using CallCenterM.CORE;
using CallCenterM.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterM.Application
{
    /// <summary>
    /// Manager de message
    /// </summary>
    public class MessageManager : GenericManager<Message>
    {
        /// <summary>
        /// Constructor del manager de Message
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public MessageManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
