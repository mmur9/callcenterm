﻿using CallCenterM.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterM.DAL
{
    /// <summary>
    /// contexto de datos
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Constructor por defecto de la clase
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        /// <summary>
        /// metodo estatico para crear el contexto
        /// </summary>
        /// <returns>Contexto de datos</returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Colección persistible de incidencias
        /// </summary>
        public DbSet<Incidence> Incidences { get; set; }

        /// <summary>
        /// Colección persisitible de mensajes para acceder directamente, y no a traves de Incidences
        /// </summary>
        public DbSet<Message> Messages { get; set; }





        /// <summary>
        /// se usa para cambiar aspectos de la DB
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            

        }
    }
}
