namespace CallCenterM.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAliasInMessage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Messages", "Alias");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Messages", "Alias", c => c.String());
        }
    }
}
