﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallCenterM.Application;
using CallCenterM.CORE;
using CallCenterM.DAL;
using Microsoft.AspNet.Identity;

namespace CallCenterM.Web.Client
{
    public partial class IncidenceCreate : System.Web.UI.Page
    {
        IncidenceManager incidenceManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            incidenceManager = new IncidenceManager(context);

            ddlType.DataSource = Enum.GetValues(typeof(IncidenceType));
            ddlType.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Incidence incidence = new Incidence
                {
                    CreatedDate = DateTime.Now,
                    Equipment = txtEquipment.Text,
                    IncenceType = (IncidenceType)Enum.Parse(typeof(IncidenceType), ddlType.SelectedValue),
                    Priority = IncidencePriority.Low,
                    Status = IncidenceStatus.Open,
                    User_Id = User.Identity.GetUserId(),

                    Messages = new List<Message>
                {
                    new Message
                    {
                        Date = DateTime.Now,
                        Text = txtIncidence.Text,
                        User_Id = User.Identity.GetUserId()
                    }
                }

                };
                incidenceManager.Add(incidence);
                incidenceManager.Context.SaveChanges();
                Response.Redirect("Incidences");
            }catch(Exception ex)
            {
                //TODO: sacar error a un log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al guardar.",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }

        }
    }
}