﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Bubble.ascx.cs" Inherits="CallCenterM.Web.Controls.Bubble" %>
<div class="bubble <%=!IsAdmin?"alt":"" %>">
    <div class="txt">
    <p class="text" runat="server"><asp:Label ID="text" runat="server" Text="Label"></asp:Label></p>
    <span class="timestamp"><asp:Label ID="date" runat="server" Text=""></asp:Label></span>
</div>
    <div class="bubble-arrow" <%=!IsAdmin?"alt":""%>></div>
</div>

