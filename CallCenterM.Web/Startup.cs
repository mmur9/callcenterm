﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallCenterM.Web.Startup))]
namespace CallCenterM.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
