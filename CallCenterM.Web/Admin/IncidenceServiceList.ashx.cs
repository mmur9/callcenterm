﻿using CallCenterM.Application;
using CallCenterM.DAL;
using CallCenterM.Web.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace CallCenterM.Web.Admin
{
    /// <summary>
    /// Summary description for IncidenceServiceList
    /// </summary>
    public class IncidenceServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["iDisplaylength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ApplicationDbContext contextdb = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(contextdb);

            #region select
            var allIncidences = incidenceManager.GetAll();
            var incidences = allIncidences
                .Select(p => new AdminIncidenceList
                {
                    Id = p.Id,
                    Message = p.Messages.FirstOrDefault().Text,
                    Equipment = p.Equipment.ToString(),
                    Date = p.CreatedDate,
                    Status = p.Status.ToString(),
                    User = p.User != null ? p.User.UserName : ""
                });
            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) || 
                               Message.ToString().Contains(@0) ||
                               Equipment.ToString().Contains(@0) ||
                               Date.ToString().Contains(@0) ||
                               Status.ToString().Contains(@0)
                               User.ToString().Contains(@0)";
                               
                incidences = incidences.Where(where, sSearch);

            }
            #endregion

            #region Paginate
            incidences = incidences
                .OrderBy(sortColum + " " + iSortDir)
                .Skip(iDisplayStart)
                .Take(iDisplayLength);

            #endregion

            var result = new
            {
                iTotalRecords = allIncidences.Count(),
                iTotalDisplayRecords = allIncidences.Count(),
                aaData = incidences
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}