﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallCenterM.Web.Admin
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userid = User.Identity.GetUserId();
            var userName = User.Identity.GetUserName();

            if (!Page.IsPostBack)
            {
                name.Text = "Pepo";
            }
        }

        protected void send_Click(object sender, EventArgs e)
        {
            result.Text = "Hola " + name.Text;
        }
    }
}