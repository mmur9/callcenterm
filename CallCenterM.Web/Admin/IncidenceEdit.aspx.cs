﻿using CallCenterM.Application;
using CallCenterM.CORE;
using CallCenterM.DAL;
using CallCenterM.Web.Controls;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallCenterM.Web.Admin
{
    public partial class IncidenceEdit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IncidenceManager incidenceManager = null;
        MessageManager messageManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            incidenceManager = new IncidenceManager(context);
            messageManager = new MessageManager(context);

            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if(int.TryParse(Request.QueryString["id"], out id))
                {
                    var incidence = incidenceManager.GetById(id);
                    if(incidence != null)
                    {
                      
                            LoadIncidence(incidence);
                        
                    }
                }
            }
        }
        private void LoadIncidence(Incidence incidence)
        {
            txtId.Value = incidence.Id.ToString();
            txtEquipment.Text = incidence.Equipment;
            txtInternalNote.Text = incidence.InternalNote;
            ddlType.Text = incidence.IncenceType.ToString();

            var content = (ContentPlaceHolder)Master.FindControl("MainContent");
            var div = content.FindControl("messages");
            div.Controls.Clear();
            foreach(var message in incidence.Messages)
            {
                var control = (Bubble)Page.LoadControl("~/Controls/Bubble.ascx");
                control.Message = message;
                div.Controls.Add(control);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try {
                var incidence = incidenceManager.GetByIdAndMessages(int.Parse(txtId.Value));

                if (String.IsNullOrWhiteSpace(txtIncidence.Text))
                {
                    var message = new Message
                    {
                        Date = DateTime.Now,
                        Text = txtIncidence.Text,
                        User_Id = User.Identity.GetUserId(),
                        Incidence_Id = int.Parse(txtId.Value)
                    };

                    incidence.Messages.Add(message);
                }
                if (!string.IsNullOrWhiteSpace(txtInternalNote.Text))
                {
                    incidence.InternalNote = txtInternalNote.Text;
                }
                context.SaveChanges();

                if(Page.IsPostBack)
                LoadIncidence(incidence);
                

            }catch(Exception ex)
            {
                //TODO: sacar error a un log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }
        }
    }
}